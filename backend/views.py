from django.shortcuts import render
from django.http import HttpResponse
import socket

def index(request):
    try:
        HOSTNAME = socket.gethostname()
    except:
        HOSTNAME = 'localhost'
    return HttpResponse("Hello, world, from {0}".format(HOSTNAME))
